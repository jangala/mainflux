# Setting up environment
to bring up stack with influx and bootstrap service
`docker-compose -f docker/docker-compose.yml -f docker/addons/influxdb-reader/docker-compose.yml -f docker/addons/influxdb-writer/docker-compose.yml -f docker/addons/bootstrap/docker-compose.yml -f docker/addons/certs/docker-compose.yml -f docker/addons/provision/docker-compose.yml up -d`

to fold down stack with influx and bootstrap service
`docker-compose -f docker/docker-compose.yml -f docker/addons/influxdb-reader/docker-compose.yml -f docker/addons/influxdb-writer/docker-compose.yml -f docker/addons/bootstrap/docker-compose.yml -f docker/addons/certs/docker-compose.yml -f docker/addons/provision/docker-compose.yml down`

if temperamental, first bring up the core services then addons in two separate commands.

# Interacting with mainflux services commands
## create a user 
`./mainflux-cli users create $userEmail $userPwd`
## get acl token for user
`./mainflux-cli users token $userEmail $userPwd`

## set token as environment variable
`token=$(./mainflux-cli users token $userEmail $userPwd | awk '{print $2}') && tokentest=$(echo ${token} | tr -d '\n')`

## read commands
`curl -s -S -i --cacert docker/ssl/certs/ca.crt -H "Authorization: $token" https://localhost/channels`

`curl -s -S -i --cacert docker/ssl/certs/ca.crt -H "Authorization: $token" https://localhost/things`

`curl -s -S -i --cacert docker/ssl/certs/ca.crt -H "Authorization: $token" http://localhost:8202/things/configs`

### Assumptions: you have downloaded and built mainflux-cli binary

#Database interaction -> see queries readme for more information

##Running queries against the db: w/ Docker exec -it
once on mainflux host server:
`docker exec -it mainflux-influxdb influx -precision rfc3339 -database 'mainflux'`

##Query formatting reference
[This] (https://influxdbcom.readthedocs.io/en/latest/content/docs/v0.9/query_language/query_syntax/)

##*Sample Queries*
get all messages for device:
`select * from messages where channel='$channelId'`

##refine the messages output to get subset of info for device by publisher or channel id example:
eg rx_rates for clients on a get box - regex to match on the last part of the topic for eg `wifi/phys/phy0/devs/wlan0/assoc/532bfe7cfb7891ff/rx_rate` 
can check in https://regex101.com/ with .*\brx_rate\b.* and dummy data

##by publisher
`select * from messages where "name" =~ /.*\brx_rate\b.*/ and publisher="$mainflux_id(ie the mqtt user id)"`
OR
##by channel
`select * from messages where "name" =~ /.*\brx_rate\b.*/ and channel='$channelId'`

# Get influxdb version information if on mainflux host:
`curl -sL -I localhost:8086/ping`
-> We are using Influx OSS

